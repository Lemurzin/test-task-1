<?php

use App\Http\Controllers\OAuthAmo;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HookController;

Route::post('hooks/handler', HookController::class);

Route::get('oauth', OAuthAmo::class);
