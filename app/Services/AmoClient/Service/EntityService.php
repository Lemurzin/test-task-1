<?php

namespace AmoClient\Service;

use Exception;
use AmoClient\AmoClient;
use AmoClient\Link\Link;
use AmoClient\Entity\Entity;
use Illuminate\Support\Facades\Http;

abstract class EntityService
{
    private $client = null;
    protected $entity = null;
    protected array $headers = [];

    public function __construct(AmoClient $client)
    {
        $this->client = $client;

        $this->headers = [
            'User-Agent' => 'amoCRM-oAuth-client/1.0',
            'Authorization' => 'Bearer '.$client->getAccessToken()
        ];
    }

    public function getById(int $id)
    {
        $link = $this->getLink($this->entity->method(), $id);

        $response = Http::accept('application/json')
            ->withHeaders($this->headers)
            ->get($link, []);
        
        if ($response->failed()) 
            new Exception($response->status());
        
        $this->entity->set($response->json());

        return $this->entity;
    }

    public function update(Entity $entity): int
    {
        $link = $this->getLink($entity->method());

        $response = Http::accept('application/json')
            ->withHeaders($this->headers)
            ->patch($link, [$entity->get()]);

        if ($response->failed()) 
            new Exception($response->status());

        return $this->entity;
    }

    public function create(Entity $entity): int 
    {
        $link = $this->getLink($entity->method());

        $response = Http::accept('application/json')
            ->withHeaders($this->headers)
            ->post($link, [$entity->get()]);

        if ($response->failed()) 
            new Exception($response->status());

        return $this->entity;
    }

    private function getLink(string $method, int $id=null): string
    {
        $link = new Link($this->client->getSubdomain());

        $link = $link->api($this->client->getVersion(), $method);

        if(isset($id)) $link->id($id);

        return $link->get();
    }
}
