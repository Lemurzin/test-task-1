<?php

namespace AmoClient\Service;

use Exception;
use AmoClient\AmoClient;
use AmoClient\Link\Link;
use AmoClient\Entity\Entity;
use AmoClient\Entity\NoteEntity;
use Illuminate\Support\Facades\Http;

class NoteService
{
    private $client = null;
    private $entity = null;
    private array $headers = [];
    
    public function __construct(AmoClient $client, Entity $parent)
    {
        $this->client = $client;
        $this->headers = [
            'User-Agent' => 'amoCRM-oAuth-client/1.0',
            'Authorization' => 'Bearer '.$client->getAccessToken()
        ];
        $this->entity = new NoteEntity($parent);
    }

    public function create(string $text, string $type = 'common')
    {
        $link = $this->getLink($this->entity->method());

        $response = Http::accept('application/json')
            ->withHeaders($this->headers)
            ->post($link, [[
                'note_type' => "$type",
                'params' => ['text' => "$text"]
            ]]);

        if ($response->failed()) 
            new Exception($response->status());
    }

    public function get()
    {
        $link = $this->getLink($this->entity->method());

        $response = Http::accept('application/json')
            ->withHeaders($this->headers)
            ->get($link, []);
        
        if ($response->failed()) 
            new Exception($response->status());
        
        $this->entity->set($response->json());

        return $this->entity;
    }

    private function getLink(string $method, int $id=null): string
    {
        $link = new Link($this->client->getSubdomain());

        $link = $link->api($this->client->getVersion(), $method);

        return $link->get();
    }
}
