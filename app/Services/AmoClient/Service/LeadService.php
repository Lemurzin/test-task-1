<?php

namespace AmoClient\Service;

use AmoClient\AmoClient;
use AmoClient\Entity\LeadEntity;

class LeadService extends EntityService
{
    public function __construct(AmoClient $client)
    {
        parent::__construct($client);
        $this->entity = new LeadEntity();
    }
}
