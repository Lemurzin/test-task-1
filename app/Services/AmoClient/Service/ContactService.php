<?php

namespace AmoClient\Service;

use AmoClient\AmoClient;
use AmoClient\Entity\ContactEntity;

class ContactService extends EntityService
{
    public function __construct(AmoClient $client)
    {
        parent::__construct($client);
        $this->entity = new ContactEntity();
    }
}
