<?php

namespace AmoClient\Token;

use Exception;
use AmoClient\Link\Link;
use Illuminate\Support\Facades\Http;

class TokenService
{
    private array $data = [];
    private string $domain;

    public function __construct(string $client_id, string $client_secret, string $domain, string $redirect_uri)
    {
        $this->data = [
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'redirect_uri' => $redirect_uri,
        ];
       
        $this->domain = $domain;
    }

    public function create(string $auth_code): array
    {
        $data = $this->data;
        $data['grant_type'] = 'authorization_code';
        $data['code'] = $auth_code;

        return $this->request($data);
    }

    public function update(string $refreshToken): array
    {
        $data = $this->data;
        $data['grant_type'] = 'refresh_token';
        $data['refresh_token'] = $refreshToken;

        return $this->request($data);
    }

    private function request(array $data)
    {
        $link = new Link($this->domain);
        
        $response = Http::accept('application/json')->withHeaders([
            'User-Agent' => 'amoCRM-oAuth-client/1.0',
        ])->post($link->auth()->get(), $data);

        if ($response->failed()) {
            new Exception($response->status().': error get token');
        } else {
            $response_data = $response->json();
            $response_data["time"] = time();
            return $response_data;
        }
    }
}
