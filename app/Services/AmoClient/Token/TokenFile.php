<?php

namespace AmoClient\Token;

use Exception;

class TokenFile 
{
    public static function getRefreshToken(): string
    {
        return self::get('refresh_token');
    }
    public static function getAccessToken(): string
    {
        return self::get('access_token');
    }
    public static function getTimeToken(): string
	{
        return self::get('time');
	}
    public static function set(array $json_array): void
    {
        file_put_contents(self::getFilePath(), json_encode($json_array, JSON_FORCE_OBJECT));
    }

    private static function get(string $key)
    {
        if (!file_exists(self::getFilePath()))
            throw new Exception('token file is empty');

        $json = file_get_contents(self::getFilePath());
        $json_array = json_decode($json, true);
        
        if(!isset($json_array[$key]))
            throw new Exception("'$key' does not exist");
           
        return $json_array[$key];
    }

    private static function getFilePath(): string
    {
        return __DIR__.'/token.json';
    }
}
