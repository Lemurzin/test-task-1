<?php
namespace AmoClient;

use AmoClient\Entity\Entity;
use AmoClient\Token\TokenFile;
use AmoClient\Token\TokenService;
use AmoClient\Service\LeadService;
use AmoClient\Service\NoteService;
use AmoClient\Service\CompanyService;
use AmoClient\Service\ContactService;

class AmoClient
{
    const VERSION = 4;
    const TIME = 86400;
    private string $client_id;
    private string $client_secret;
    private string $domain;
    private string $redirect_uri;


    public function __construct(string $client_secret, string $client_id, string $domain, string $redirect_uri)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->domain = $domain;
        $this->redirect_uri = $redirect_uri;
    }

    public function authByCode($authCode): void
    {
        $token = new TokenService($this->client_id, $this->client_secret, $this->domain, $this->redirect_uri);
        $response = $token->create($authCode);
        TokenFile::set($response);
    }
    
    public function company(): CompanyService
    {
        return new CompanyService($this);
    }

    public function contact(): ContactService
    {
        return new ContactService($this);
    }

    public function lead(): LeadService
    {
        return new LeadService($this);
    }

    public function note(Entity $parentEntity): NoteService
    {
        return new NoteService($this, $parentEntity);
    }

    public function getSubdomain(): string
    {
        return $this->domain;
    }

    public function getVersion(): string
    {
        return 'v'.self::VERSION;
    }

    public function getAccessToken(): string
    {
        $label = TokenFile::getTimeToken();

        $difference = time() - (int)$label;

        if ($difference > self::TIME)
        {
            $token = new TokenService($this->client_id, $this->client_secret, $this->domain, $this->redirect_uri);
            $response = $token->update(TokenFile::getRefreshToken());
            $accessToken = $response['access_token'];
            TokenFile::set($response);
        } else {
            $accessToken = TokenFile::getAccessToken();
        }
        return $accessToken;
    }
}
