<?php

namespace AmoClient\Link;

class Link
{
    private string $domain = '',
                   $link;
    private int $id;
    
    public function __construct(string $domain)
    {
        $this->domain = "https://$domain.amocrm.ru";
    }
    public function api($version, $method)
    {
        $this->link = "$this->domain/api/$version/$method";
        return $this;
    }
    public function auth() 
    {
        $this->link = "$this->domain/oauth2/access_token";
        return $this;
    }
    public function id(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function get(): string
    {
        $link = $this->link;
        
        if(!isset($link))
            $link = "$this->domain/";
        
        if(isset($this->id))
            $link = "$link/$this->id";

        return $link;
    }
}
