<?php

namespace AmoClient\Entity;

use AmoClient\Entity\Entity;

final class NoteEntity
{
    protected string $method = "notes";
    protected string $parent_method = "";
    protected int $entity_id;
    protected array $data = [];

    public function __construct(Entity $parent_entity)
    {
        $this->entity_id = $parent_entity->id;
        $this->parent_method = $parent_entity->method();
    }

    public function method()
    {
        if(isset($this->entity_id)) 
            $entity_id =  "/$this->entity_id";
        else 
            $entity_id =  "";

        $method = "$this->parent_method"."/"."$this->entity_id"."/"."$this->method";
        return $method;
    }

    public function get()
    {
        $res = [];

        if (!isset($this->data['_embedded'])) return [];
        if (!isset($this->data['_embedded'][$this->method])) return [];

        foreach ($this->data['_embedded'][$this->method] as $value) {
            array_push($res, [
                'id' => $value['id'],
                'params' => $value['params'],
                'responsible_user_id' => $value['responsible_user_id'],
                'entity_id' => $value['entity_id'],
                'note_type' => $value['entity_id'],
            ]);
        }

        return $res;
    }

    public function set(array $data)
    {
        $this->data = $data;
        return $this;
    }
}