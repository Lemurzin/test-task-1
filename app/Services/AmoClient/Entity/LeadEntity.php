<?php

namespace AmoClient\Entity;

use AmoClient\Entity\Entity;

final class LeadEntity extends Entity
{
    protected string $method = 'leads';
    protected array $params = ['id', 'name', 'price', 'responsible_user_id', 'status_id', 'pipeline_id'];
}