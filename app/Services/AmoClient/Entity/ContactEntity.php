<?php

namespace AmoClient\Entity;

use AmoClient\Entity\Entity;

final class ContactEntity extends Entity
{
    protected string $method = "contacts";
    protected array $params = ['id', 'name', 'first_name', 'last_name', 'responsible_user_id'];
}