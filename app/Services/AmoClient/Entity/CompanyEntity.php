<?php

namespace AmoClient\Entity;

use AmoClient\Entity\Entity;

final class CompanyEntity extends Entity
{
    protected string $method = "companies";
    protected array $params = ['id', 'name', 'responsible_user_id'];
}