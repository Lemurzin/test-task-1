<?php

namespace AmoClient\Entity;

use Exception;

abstract class Entity
{
    protected string $method;
    protected array $params = [], 
                    $data = [];

    public function __set($property, $value)
    {
        if (array_key_exists($property, $this->params))
            $this->data[$property] = $value;
        
        return $this;
    }
    
    public function __get($property)
    {
        if(isset($this->data[$property]))
            return $this->data[$property];

        if (array_key_exists($property, $this->data['_embedded']) !== false) {
            $ids = [];

          
            foreach ($this->data['_embedded'][$property] as $value) {
                array_push($ids, $value['id']);
            }

            return $ids;
        }     

        throw new Exception("'$property' does not exist");
    }
    public function method()
    {
        return $this->method;
    }

    public function cf()
    {
        $entity_cf = [];
        
        if (!$this->data) return $entity_cf;
        if (!isset($this->data['custom_fields_values'])) return $entity_cf;

        foreach ($this->data['custom_fields_values'] as $value) {
            $entity_cf[$value['field_id']] = [
                'name' => $value['field_name'],
                'value' => $value['values'][0]['value']
            ];
        }
        return $entity_cf;
    }
    
    public function get()
    {
        $res = [];
        foreach ($this->data as $key => $value) {
            if (array_search($key, $this->params) !== false) {
                $res[$key] = $value;
            }
        }
        return $res;
    }

    public function set(array $data)
    {
        $this->data = $data;
        return $this;
    }
}
