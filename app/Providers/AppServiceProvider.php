<?php

namespace App\Providers;

use AmoClient\AmoClient;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AmoClient::class, function ($app){
            return new AmoClient(
                config('amocrm.client_secret'),
                config('amocrm.client_id'),
                config('amocrm.domain'),
                config('amocrm.redirect_uri')
            );
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
