<?php

namespace App\Jobs;

use AmoClient\AmoClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class HookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const FIELDS_CHECKED = [
        'contact' => [],
        'lead' => ['price']
    ];
    
    private $hook;

    public function __construct($hook)
    {
        $this->hook = $hook;
    }

    public function handle(AmoClient $client): void
    {
        if (isset($this->hook['leads'])) {
            $hook = $this->hook['leads'];
            $checked = self::FIELDS_CHECKED['lead'];
            $service = $client->lead();
        }
        else if (isset($this->hook['contacts'])) {
            $hook = $this->hook['contacts'];
            $checked = self::FIELDS_CHECKED['contact'];
            $service = $client->contact();
        }

        if (isset($hook['add'])) {
            $data = $hook['add'][0];

            $entity = $service->getById($data['id']);

            $note_service = $client->note($entity);
            $note_service->create(
                'Название: '.$data['name'].', '.
                'Отв-ный: '.$data['responsible_user_id'].', '.
                'Дата создания: '.date('d-m-Y H:i:s', $data['last_modified'])
            );

        } else if (isset($hook['update'])) {

            $data = $hook['update'][0];

            $entity = $service->getById($data['id']);
            $note_service = $client->note($entity);

            $update_fields = $this->getUpdateCF($entity->cf(), $data);

            $update_checked = '';
            foreach ($checked as $value) {
               
                if (!isset($data[$value])) continue;
                $entity_data = $entity->get();

                if (!isset($entity_data[$value])) {
                    $update_checked .= $value.':'.$data[$value];
                    continue;
                }

                //if ($entity_data[$value] != $data[$value])
                    $update_checked .= $value.':'.$data[$value];
            }

            $update_fields .= $update_checked;
            if (!$update_fields) return;

            $note_service->create(
                'Название: '.$data['name'].', '.
                'Обновленные поля: '.$update_fields.', '.
                'Дата создания: '.date('d-m-Y H:i:s', $data['last_modified'])
            );
        }
    }
    private function getUpdateCF($entity_cf, $hook) {
        $update_fields = '';

        $hook_cf = [];  
    
        if (!isset($hook['custom_fields'])) return $update_fields;
        
        foreach ($hook['custom_fields'] as $value) {
            $hook_cf[$value['id']] = [
                'name' => $value['name'],
                'value' => $value['values'][0]['value']
            ];
        }

        foreach ($hook_cf as $key => $value) {
            if (!isset($entity_cf[$key])) {
                $update_fields .= $value['name'].': '.$value['value'].' ';
                continue;
            }
            // if ($hook_cf[$key]['value'] != $entity_cf[$key]['value'])
                $update_fields .= $value['name'].': '.$value['value'].' ';
        }
    
        if ($update_fields) $update_fields = '('.$update_fields.') ';

        return $update_fields;
    }

}
