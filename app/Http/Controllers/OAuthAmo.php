<?php

namespace App\Http\Controllers;

use AmoClient\AmoClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OAuthAmo extends Controller
{
    public function __invoke(Request $request, AmoClient $client)
    {   
        $data = $request->validate([
            '*.code' => 'required|string'
        ]);

        $client->authByCode($data['code']);
    }
}