<?php

namespace App\Http\Controllers;

use App\Jobs\HookJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HookController extends Controller
{
    public function __invoke(Request $request)
    {
        Log::channel('hook')->info($request); 

        if (!isset($request['account'])) return;
        if (config('amocrm.domain') != $request['account']['subdomain']) return;


        if (isset($request['leads']) || isset($request['contacts'])) { 
            HookJob::dispatch($request->all());
        }
    }
}